# App Links

It is often desirable to have custom scheme mechanisms registered on the operating systems (Windows, Mac, Linux) so your application can be launched from a website or other tools.

In order to do this you need to register a custom URI to be bound to your application instance.

## Windows

The build process for Tauri uses a bundler standard called WIX which enables the creation of an installer package that produces an MSI. This WIX standard allows us to manipulate the Registry during installation to setup a custom URI so that when the custom URI is invoked it will launch our application.

Reference Documents:
* [MS Registering an Application to URI Scheme](https://docs.microsoft.com/en-us/previous-versions/windows/internet-explorer/ie-developer/platform-apis/aa767914(v=vs.85)?redirectedfrom=MSDN)
* [Creating Registry Settings with WIX](http://www.p-nand-q.com/programming/windows/wix/registry.html)
* [WIX Reference](https://wixtoolset.org/documentation/manual/v4/reference/)

### Using a Template

In this example package we will be using a full template replacement based on the default tauri provided templated (https://github.com/tauri-apps/tauri/blob/dev/tooling/bundler/src/bundle/windows/templates/main.wxs). We will be making a copy of this and injecting the WIX block needed to register our new URI scheme.

In order to register a new URI scheme for this application we will need to create the registry values. These registry values need to be handled under the `HKEY_CLASSES_ROOT` context.

```
<Component Id="ApplicationURI" Guid="*">
    <RegistryKey Root="HKCR" Key="acme">
        <RegistryValue Type="string" Value="acme" />
        <RegistryValue Name="URL Protocol" Type="string" Value=""/>
        <RegistryKey Key="shell">
            <RegistryKey Key="open">
                <RegistryKey Key="command">
                    <RegistryValue Type="string" Value='[!Path] "%1"'/>
                </RegistryKey>
            </RegistryKey>
        </RegistryKey>
    </RegistryKey>
</Component>
```

This will create the Registry path of `Computer\HKEY_CLASSES_ROOT\acme\shell\open\command`. This tells windows that when the URL scheme `acme:` is called then it should invoke our Application EXE.

`[!Path]` is a contextual variable that is created during the installation based on the path the user selects for their installation.

This component needs to be added to a DirectoryRef object that is in the HKEY_CLASSES_ROOT (HKCR) Context as opposed to HKEY_CLASSES_CURRENT_USER (HKCU) so in this demo we have chosen to drop this component under the DirectoryRef tag (`<DirectoryRef Id="INSTALLDIR">` on line 85 of the template)

Once we have the component in a DirectoryRef tag we have to enable the component into a "Feature". Since these Application URIs are akin to a Shortcut we put the component into that feature tag.

```
<Feature Id="ShortcutsFeature"
    Title="Shortcuts"
    Level="1">
    ...
    <ComponentRef Id="ApplicationURI" />
    ...
</Feature>
```

With these peices in place you can build the release MSI `tauri build` then installed the MSI (under `target\\release\\bundle\\msi\\applinks_0.1.0_x64.msi`). Once installed you can test it by invoking that URI Scheme by opening the Run Dialog (`Windows Key + R`) and enter the text `acme:test` and the AppLinks application will open it.

## Mac Custom URI

TODO

## Linux Custom URI

TODO

### Single Instance Flow

When you set up the Custom URI scheme you may have noticed that the key in the REGISTRY was set to `acme/shell/open/command` meaning that a new instance is going to be launched when the Operating system is encountering that custom URI. If you notice in our Tauri setup script we launch a gRPC server that is listening on an ipv6 loopback `[::1]` with a random port designation `:0` so that we do not conflict or have to mandate a particular port. This gRPC server will be used to communicate data from the new instance to the existing instance (TBD gRPC security on this server instance).

As we start up the tauri application we check if there are any existing processes by the applications name currently running. If a process by the applications name (`applinks.exe` in our example) then we will scan the open sockets for the listening port ip address binding and port used.

We then use this information to open up a client to the listening ip:port and send over the startup arguments of the new instance. On the existing instance, we see this request then emit the message to the UI logic via the `event.emit` mechanism in Tauri.

This is an example of just passing the command arguments into the existing instance when a new one is launched. It has its uses as is but a more robust example will become available _(TODO Update when available)_ that shows an OAuth2 login flow.

# Thanks

This sample and work was thanks to the following members of the Tauri Apps discord

* ttay24#1782
* FabianLars#6748
* lucasfernog#1488
